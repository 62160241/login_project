/* eslint-disable space-before-function-paren */
import api from './api'

export function getProduct() {
  return api.get('/products')
}
